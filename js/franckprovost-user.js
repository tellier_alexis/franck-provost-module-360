/**
 * Namespace Coiff&Co
 */
var FPROVOST = {
	
	init: function(){
		FPROVOST.Docebo.init();
	},
	
	isMobile: function(){
		return (navigator.userAgent.match(/Android/i)
				|| navigator.userAgent.match(/webOS/i)
				|| navigator.userAgent.match(/iPhone/i)
				|| navigator.userAgent.match(/iPad/i)
				|| navigator.userAgent.match(/iPod/i)
				|| navigator.userAgent.match(/BlackBerry/i)
				|| navigator.userAgent.match(/Windows Phone/i)
		);
	},
	
	/**
	 * url without hash nor query string
	 */
	baseUrl: function(){
		return document.location.href.replace(document.location.search, '').replace(document.location.hash, '').replace('/index.html', '');
	},
	
	/**
	 * Docebo's call methods & data
	 */
	Docebo: {
		/**
		 * @type Int --> got from "document.location" properties/param
		 */
		id_user: null,
		/**
		 * @type string Hash code
		 */
		auth_code: null,
		
		/**
		 * Ajax Url returning JSON including the launch_url for given quiz id_org
		 * @type string
		 * @see "setAuthCode"
		 */
		apiQuizUrl: 'https://remote.yesnyou.com/api-gen/public/generic-provo.php',
		
		Quizes: [],
		
		/**
		 * We have to take the quiz map in docebo.js
		 * And for each quiz, get the e-learning URL (launch_url)
		 * Replacing the "auth_code" and the "id_user" parameters
		 */
		init: function(){
			this.setUserId();
			this.setAuthCode();
		},
		

		/**
		 * Set "id_user" property, got from URL
		 */
		setUserId: function(){
			var querystring = location.search.substring(1);//Searched on top window
			if (querystring.indexOf('id_user=')===-1){
				//Search into iframes URL
				querystring = FPROVOST.Docebo.getParentWindowUrl(window, 0);
			}
			var parameters = querystring.split('&');
			if (!parameters.length) return false;
			var args = {};
			for (var i in parameters){
				var props = parameters[i].split('=');
				if (props.length){
					if (props[0]==='id_user'){
						FPROVOST.Docebo.id_user = props[1];
						// console.log(FPROVOST.Docebo.id_user);
						return true;
					}
				}
			}
			return false;
		},
		
		getParentWindowUrl: function(currentWindow, iteration){
			iteration++;
			if (iteration > 3) return '';//Max loop: 3
			
			var parentWindow = $(currentWindow).get(0).parent;
			if (typeof parentWindow === 'undefined') return '';

			var querystring = '';
			if ('location' in parentWindow)
				querystring = parentWindow.location.search.substring(1);
			
			if (querystring.indexOf('id_user=')===-1){
				querystring = FPROVOST.Docebo.getParentWindowUrl(parentWindow, iteration);
			}
			// console.log('parentWindow',parentWindow);
			// console.log('currentWindow',currentWindow);
			// console.log('querystring',querystring);
			return querystring;
		},
		
		/**
		 * 
		 * @returns boolean true/false
		 */
		setAuthCode: function(){
			if (!FPROVOST.Docebo.id_user){
				if (!FPROVOST.Docebo.setUserId()){
					console.log('#Error: Could not find the "id_user"');
					return false;
				}
			}
			
			if (typeof FPROVOST_Quizes === 'undefined'){
				console.log('#Fatal Error: missing required file and variable FPROVOST_Quizes');
				return false;
			}
			//Get the 1st quiz having a auth_code in its launch_url
			for (var ix in FPROVOST_Quizes){
				var Quiz = FPROVOST_Quizes[ix];
				if (typeof Quiz.launch_url !== 'undefined' && Quiz.launch_url.indexOf('auth_code')>-1){
					//AJAX request
					$.ajax({
						url: FPROVOST.Docebo.apiQuizUrl,
						method: 'POST',
						dataType: 'json',
						data: {
							method: 'organization/play',
							id_user: FPROVOST.Docebo.id_user,
							id_org: Quiz.id_org
						},
						success: function(json){
							if (typeof json.launch_url !== 'undefined' && json.launch_url.indexOf('auth_code')>-1){
								var strFirstPart = json.launch_url.substring(json.launch_url.indexOf('auth_code'));
								indexOfSep = strFirstPart.indexOf('&');
								if (indexOfSep > 0){
									strFirstPart = strFirstPart.substring(0, indexOfSep);
								}
								//Set auth_code
								FPROVOST.Docebo.auth_code = strFirstPart.replace('auth_code=', '');
								//Set Quizes
								FPROVOST.Docebo.setQuizes();
							}
							else
								console.log('#Error: could not find the user/quiz auth_code');
						}
					});
					return true;
				}
			}
			return false;
		},
		
		setQuizes: function(){
			if (!FPROVOST.Docebo.id_user || !FPROVOST.Docebo.auth_code){
				console.log('#Error: missing auth_code or id_user for quiz settings');
				return false;
			}
			//Fill in and complete quiz data
			for (var ix in FPROVOST_Quizes){
				var Quiz = FPROVOST_Quizes[ix];
				this.Quizes[ix] = Quiz;
				this.Quizes[ix].launch_url = Quiz.launch_url.
					replace('%id_user%', FPROVOST.Docebo.id_user).
					replace('%auth_code%', FPROVOST.Docebo.auth_code);
				//The video got fluctuant url --> load it in any case
				if (Quiz.launch_url.indexOf('.mp4')>-1){
					$.ajax({
						url: FPROVOST.Docebo.apiQuizUrl,
						method: 'POST',
						dataType: 'json',
						data: {
							method: 'organization/play',
							id_user: FPROVOST.Docebo.id_user,
							id_org: Quiz.id_org
						},
						success: function(json){
							if (typeof json.launch_url !== 'undefined'){
								Quiz.launch_url = json.launch_url;
							}
						}
					});
				}
			}
			//Place the good quiz in the good location
			this.goodQuizIsDeadQuiz();
		},
		
		/**
		 * Debug: random
		 */
		goodQuizIsDeadQuiz: function(){
			if (typeof FPROVOST_Quiz_Map === 'undefined'){
				console.log('#Fatal Error: missing required file and variable FPROVOST_Quiz_Map');
				return false;
			}
			if (!this.Quizes.length){
				console.log('#Fatal Error: FPROVOST.Docebo.Quizes array is empty');
				return false;
			}

			for (var quizid in FPROVOST_Quiz_Map){
				var quiztitle = FPROVOST_Quiz_Map[quizid].toLowerCase().trim();
				if (!quiztitle) continue;
				for (var ix in this.Quizes){
					var theQuiz = this.Quizes[ix];
					if (theQuiz.title.toLowerCase().trim()==quiztitle){
						var launch_url = theQuiz.launch_url;

						if (launch_url.indexOf('.mp4')>-1){
							$(quizid).attr('data-href', launch_url);
							//It's a mp4 direct video link: embed it into a video tag							
							$('.video-material').click(function(){
								var colDroiteTarget = $(this).data('href');
								$("#container_video").empty();
								$("#container_video").append(
									'<div id="video_player_container" class="fullscreen">'+
										'<video id="video_player_fullscreen" preload="metadata" controls></video>'+
									'</div>'
								);
								// console.log(colDroiteTarget);
								$('#video_player_fullscreen').attr('src', colDroiteTarget).get(0).play();
								
								return false;

							});
							

							$('.video.bootpopup').click(function(){
									
								var VideoBoot = $(this).data('href');
								var modalClose = $(this).data('target');
								$("#modalContentVideo").empty();
								$("#modalContentVideo").append(
									'<div id="video_player_container" class="fullscreen">'+
										'<video id="video_player_fullscreen" preload="metadata" controls></video>'+
									'</div>'
								);
								$('#video_player_fullscreen').attr('src', VideoBoot).get(0);
								if(modalClose.length) {
									$(modalClose).removeClass('in').css('display','none');
									$('.modal-backdrop.backpopup').remove();
								} 

								$("#myModalVideo").modal({show:true});
								$('.modal-backdrop').addClass('backvideo');
								return false;
							});
						
						}
						else{
							$(quizid).attr('href', launch_url);
							$('.bootpopup.quiz').click(function(){
								$("#modalContentQuiz").empty();
								idBoot = $(this).attr("id");
								
								// if(idBoot === "video_rituel_orient") {
								// 	var VideoBoot = $(this).data('href');
								// 	$("#myModal").addClass('popup-video');
									
								// 	$("#modalContent").append(
								// 		'<div id="video_player_container" class="fullscreen">'+
								// 			'<video id="video_player_fullscreen" preload="metadata" controls></video>'+
								// 		'</div>'
								// 	);

								// 	$('#video_player_fullscreen').attr('src', VideoBoot).get(0).play();
								// }
								// else {
									$("#modalContentQuiz").append('<iframe src="" id="myFrame" style="zoom:1" frameborder="0" height="99.6%" width="99.6%"></iframe>');
									var frametarget = $(this).attr('href');
									
									$('iframe').attr("src", frametarget ); 		
								// }

								$("#myModalQuiz").modal({show:true});	
								$('.modal-backdrop').addClass('backquiz');					
								

								return false;
							});
							
							$('.slideshow').click(function(){
								$("#modalContentAuthoring").empty(); 		 
								$("#modalContentAuthoring").append('<iframe src="" id="myFrame" style="zoom:1" frameborder="0" height="99.6%" width="99.6%"></iframe>');
								var frametarget = $(this).attr('href');
								
								$('iframe').attr("src", frametarget ); 
								$("#myModalAuthoring").modal({show:true});
								$('.modal-backdrop').addClass('backauthoring');

								return false;
							});

						}						
						break;
					}
					
				}
			}
		}
	}
};

$(function(){
	FPROVOST.init();
});