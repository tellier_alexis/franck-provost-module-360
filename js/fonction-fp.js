function loadvitrine(id) {  
	
	$("#modalContentVitrine").empty();
	//append page content to #modalContentVitrine
	var filePathD = 'content/vitrine/'+id+'.html'; 

	$.ajax( filePathD )
	.done(function(data) {					    
	    $("#modalContentVitrine").append(data);
	    $('#myModalVitrine').modal('show');
	    $('.modal-backdrop').removeClass('backvitrine backpopup backblack backvideo');
	    $('.modal-backdrop').addClass('backvitrine');
	  })
	  .fail(function() {
	    console.log( "error loading",filePathD );
	  })
	  .always(function() {
	    // console.log( "done",filePathD );
	  });
	FPROVOST.init();

}
function loadContent(id) {  
				
	$("#extraContent").empty();   
	//append page content to #extraContent
	var filePathA = 'content/include/'+id+'.html'; 


	$.ajax( filePathA )
	.done(function(data) {					    
	    $("#extraContent").append(data);
	    //$('#myModal').modal('show');
	  })
	  .fail(function() {
	    // console.log( "error loading",filePathA );
	  })
	  .always(function() {
	  	$("#container").addClass('side-open');
		$("#myModalBlack").addClass('side-open');
	    // console.log( "done",filePathA );
	  });
	FPROVOST.init();
}
function loadPopup(id) {  
				
	$("#modalContentPopup").empty();

	//append page content to #modalContentBlack
	var filePathB = 'content/popup/'+id+'.html'; 

	$.ajax( filePathB )
	.done(function(data) {					    
	    $("#modalContentPopup").append(data);
	    $('#myModalPopup').modal('show');
	    $('.modal-backdrop').removeClass('backvitrine backpopup backblack backvideo');
	    $('.modal-backdrop').addClass('backpopup');
	  })
	  .fail(function() {
	  
	    console.log( "error loading",filePathB );
	  })
	  .always(function() {
	    // console.log( "done",filePathB );
	  });
	FPROVOST.init();

}
function loadBP(id) {  
				
	$("#modalContentBlack").empty();
	$("#myModalBlack").removeClass('panel_bp_diagnos_shampooing');

	//append page content to #modalContentBlack
	var filePathC = 'content/black-panel/'+id+'.html'; 
	if( id === "bp_diagnos_shampooing") {
		$('#myModalBlack').addClass("panel_"+id);
	}
	$.ajax( filePathC )
	.done(function(data) {					    
	    $("#modalContentBlack").append(data);
	    $('#myModalBlack').modal('show');
	    $('.modal-backdrop').removeClass('backvitrine backpopup backblack backvideo');
	    $('.modal-backdrop').addClass('backblack');
	  })
	  .fail(function() {
	    console.log( "error loading",filePathC );
	  })
	  .always(function() {
	    // console.log( "done",filePathC );
	  });
	FPROVOST.init();

}

var loadSlider = (function() {

    var that = {};

    that.init = function () {
    	$('#slideExtraContent').bxSlider({
		  	pager: false,
		  	nextText:"Page suivante >",
		  	prevText:"",
			nextSelector: "#pagerSlideExtraContent #next",
			prevSelector: "#pagerSlideExtraContent #prev",
			adaptiveHeight: true,
			infiniteLoop: false,
			hideControlOnEnd: true,
			onSlideBefore: function(){
				$('#video_player_fullscreen').each(function(){
					this.pause();
				});
			}
			

		});
		$('#next a').addClass('btn-fp');
		$('#prev a').addClass('btn-fp');
	
    }

    return that;

})();


var heightContent = (function() {

    var that = {};

    that.init = function () {
    	$( window ).resize(function() {
			var windowHeight = $(this).height();
			var extraContentHeaderHeight = $("#extraContentHeader").height();
			var extraContentHeaderMargin = 30;
			var extraContentFooterHeight = 75;

			var extraContentContentFinal  = Math.round((windowHeight - extraContentHeaderHeight - extraContentHeaderMargin - extraContentFooterHeight ) *100) /100;
			var extraContentHeaderHeightFinal = Math.round(extraContentHeaderHeight *100)/100 + extraContentHeaderMargin;
			
			$('#extraContent .content').css('height', extraContentContentFinal).css('marginTop', extraContentHeaderHeightFinal).css('paddingBottom', extraContentFooterHeight);
		}).resize();
    }

    return that;

})();

// $ = jQuery;
$(document).ready(function(){
	$('#slider-intro').bxSlider({
	  	pager: false,
	  	nextText:">",
	  	prevText:"<",
		nextSelector: "#pagerSlideIntro #next",
		prevSelector: "#pagerSlideIntro #prev",
		adaptiveHeight: true,
		infiniteLoop: false,
		hideControlOnEnd: true,
		onSlideBefore: function(){
			$('#video_player_fullscreen').each(function(){
				this.pause();
			});
		}
	});	
	$("#mobile_overlay").click(function(){
		$("#container").removeClass('side-open');
		$("#myModalBlack").removeClass('side-open');
	  	// $("#myModal").removeClass('side-open');
	  	// $("#myModalVitrine").removeClass('side-open');
	});


});