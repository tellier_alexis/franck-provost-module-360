// $ = jQuery;
$(document).ready(function(){
	$('#slider-intro').bxSlider({
	  	pager: false,
	  	nextText:">",
	  	prevText:"<",
		nextSelector: "#pagerSlideIntro #next",
		prevSelector: "#pagerSlideIntro #prev",
		adaptiveHeight: true,
		infiniteLoop: false,
		hideControlOnEnd: true,
		onSlideBefore: function(){
			$('#video_player_fullscreen').each(function(){
				this.pause();
			});
		}
	});	
	$("#mobile_overlay").click(function(){
		$("#container").removeClass('side-open');
		$("#myModalBlack").removeClass('side-open');
	  	// $("#myModal").removeClass('side-open');
	  	// $("#myModalVitrine").removeClass('side-open');
	});

});